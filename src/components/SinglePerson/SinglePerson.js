import React, { Component } from "react";
import styles from "./SinglePerson.module.css";

export default class SinglePerson extends Component {
  render() {
    const { person, handleInfo } = this.props;

    return (
      <div
        className={styles.container}
        onClick={() => handleInfo(person.created)}
      >
        <p>{person.name}</p>
      </div>
    );
  }
}
