import React, { Component } from "react";
import { getData } from "../../api";
import { BASE_URL_PEOPLE } from "../../constants";
import Pagination from "../Pagination";
import SinglePersonInfo from "../SinglePersonInfo/SinglePersonInfo";
import SinglePerson from "./../SinglePerson";
import styles from "./ListPeople.module.css";

class ListPeople extends Component {
  state = {
    data: {
      results: [],
      count: 0,
    },
    currentPage: 1,
    loading: false,
    personInfo: {
      status: false,
      person: {},
    },
  };

  handleInfo = (id) => {
    const { results } = this.state.data;

    results.map((el) => {
      if (el.created === id) {
        return this.setState({
          personInfo: { person: el, status: !this.state.personInfo.status },
        });
      }
    });
  };

  componentDidMount() {
    this.setState({ loading: true });
    getData(`${BASE_URL_PEOPLE}${this.state.currentPage}`)
      .then((data) => this.setState({ data }))
      .finally(() => this.setState({ loading: false }));
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevState.currentPage !== this.state.currentPage) {
      getData(`${BASE_URL_PEOPLE}${this.state.currentPage}`)
        .then((data) => this.setState({ data }))
        .finally(() => this.setState({ loading: false }));
    }
  }

  changePage = (page) => {
    this.setState({ currentPage: page });
  };

  render() {
    const { loading, data, personInfo, currentPage } = this.state;

    const count = Math.ceil(data.count / 10);
    return (
      <>
        {loading ? (
          <div>Loading..</div>
        ) : personInfo.status ? (
          <SinglePersonInfo
            person={this.state.personInfo.person}
            handleInfo={this.handleInfo}
          />
        ) : (
          <>
            <div className={styles.container}>
              {data.results.map((el) => (
                <SinglePerson
                  key={el.created}
                  handleInfo={this.handleInfo}
                  person={el}
                />
              ))}
            </div>
            <div className={styles.pagination}>
              {Array(count + 4)
                .fill(null)
                .map((el, i) => (
                  <Pagination
                    key={Math.random()}
                    changePage={this.changePage}
                    currentPage={currentPage}
                    i={i}
                  />
                ))}
            </div>
          </>
        )}
      </>
    );
  }
}

export default ListPeople;
