import React, { Component } from "react";
import styles from "./SinglePersonInfo.module.css";

export default class SinglePersonInfo extends Component {
  render() {
    const { handleInfo, person } = this.props;
    const {
      name,
      birth_year,
      gender,
      height,
      skin_color,
      hair_color,
      eye_color,
      created,
    } = person;
    return (
      <>
        <div className={styles.btnContainer}>
          <button onClick={() => handleInfo(created)}> ← Back</button>
        </div>
        <div className={styles.nameContainer}>{name}</div>
        <div className={styles.pContainer}>
          <p>
            Birth Year:   <b>{birth_year}</b>
          </p>
          <p>
            Gender: <b>{gender}</b>
          </p>
          <p>
            Height: <b>{height}</b>
          </p>
          <p>
            Skin Color: <b>{skin_color}</b>
          </p>
          <p>
            Hair Color:<b>{hair_color}</b>
          </p>
          <p>
            Eye Color: <b>{eye_color}</b>
          </p>
          <div className={styles.nameContainer}></div>
        </div>
      </>
    );
  }
}
