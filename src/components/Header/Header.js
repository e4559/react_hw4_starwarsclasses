import React, { Component } from "react";
import styles from "./Header.module.css";

export class Header extends Component {
  render() {
    return (
      <header>
        <div className={styles.container}>Header</div>
      </header>
    );
  }
}

export default Header;
