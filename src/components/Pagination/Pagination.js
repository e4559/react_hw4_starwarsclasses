import React, { Component } from "react";
import styles from "./Pagination.module.css";

export default class Pagination extends Component {
  render() {
    const { changePage, i, currentPage } = this.props;

    if (i == 0) {
      return (
        <button className={styles.btn} onClick={() => changePage(1)}>
          ˂˂
        </button>
      );
    } else if (i == 1) {
      return (
        <button
          className={styles.btn}
          onClick={() => currentPage > 1 && changePage(currentPage - 1)}
        >
          ˂
        </button>
      );
    } else if (i == 11) {
      return (
        <button
          className={styles.btn}
          onClick={() => currentPage < 9 && changePage(currentPage + 1)}
        >
          ˃
        </button>
      );
    } else if (i == 12) {
      return (
        <button className={styles.btn} onClick={() => changePage(9)}>
          ˃˃
        </button>
      );
    } else {
      return (
        <button
          className={currentPage == i - 1 ? styles.btnActive : styles.btn}
          onClick={() => changePage(i - 1)}
        >
          {i - 1}
        </button>
      );
    }
  }
}
