import React, { Component } from "react";
import ListPeople from "../ListPeople/ListPeople";
import styles from "./MainPage.module.css";

export default class MainPage extends Component {
  render() {
    return (
      <div className={styles.container}>
        {/* <button>People</button> */}
        <ListPeople />
      </div>
    );
  }
}

